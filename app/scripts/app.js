'use strict';

angular.module('myApp', ['ui.router', 'ngResource'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            // route for the home page
            .state('app', {
                url:'/',
                views: {                    
                    'content': {
                        templateUrl : 'views/homepage.html',
                        //controller  : 'IndexController'
                    }
                }

            })

            // route for the aboutus page
            .state('app.home', {
                url:'home',
                views: {
                    'header@': {
                        template: '',
                    },
                    'content@': {
                        templateUrl : 'views/home.html',
                        // controller  : 'AboutController'
                    }
                }
            })

            // route for the aboutus page
            .state('app.aboutus', {
                url:'aboutus',
                views: {
                    'content@': {
                        templateUrl : 'views/aboutus.html',
                        // controller  : 'AboutController'
                    }
                }
            })

            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        // controller  : 'ContactController'
                    }
                }
            })

            // route for the menu page
            .state('app.sharing', {
                url: 'sharing',
                views: {
                    'header@': {
                        template: '',
                    },
                    'content@': {
                        templateUrl : 'views/sharing.html',
                        // controller  : 'MenuController'
                    }
                }
            })

            // route for the dishdetail page
            .state('app.starred', {
                url: '/starred',
                views: {
                    'header@': {
                        template: '',
                    },
                    'content@': {
                        templateUrl : 'views/starred.html',
                        // controller  : 'DishDetailController'
                   }
                }
            });

        $urlRouterProvider.otherwise('/');
})
;
