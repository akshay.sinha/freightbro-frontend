'use strict';

var app = angular.module('myApp');

app.controller('registerController', function($scope, registerService) {
  $scope.register = {
    username: '',
    password: ''
  };
  $scope.registerUser = function() {
    registerService.save($scope.register).$promise.then(
      function(response) {
        $scope.regMessage = "User successfully registered.";
      },
      function(response) {
        $scope.regMessage = "There was an error.";
      }
    )
  }
})

.controller('loginController', function($scope, $state, $window, loginService) {
  $scope.login = {
    username: '',
    password: ''
  };
  $scope.disableLoginBtn = false;

  $scope.loginUser = function () {
    $scope.disableLoginBtn = true;
    loginService.save($scope.login).$promise.then(
      function(response) {
        $window.localStorage.setItem('token', angular.toJson(response.token));
        $state.go('app.home');
      },
      function(response) {
        $scope.disableLoginBtn = false;
        console.log(response);
        if(response.data) {
          $scope.loginError = response.data;
          $scope.hasLoginError = true;
        }
      }
    )
  }
})

.controller('projectController', function($scope, $window, projectService, userService) {
  $scope.token = angular.fromJson($window.localStorage.getItem('token'));

  $scope.method = {
    url: '',
    method: '',
    requestBody: '',
    responseBody: '',
    optionalHeaders: ''
  };

  $scope.user = {
    userid: '',
    permissions: ''
  };

  $scope.userslist = [];
  userService.users($scope.token).query().$promise.then(
    function(response) {
      $scope.userslist = response;
    },
    function(response) {
      console.log("Could not get users list!");
    }
  );

  
  projectService.projects($scope.token).query().$promise.then(
    function(response) {
      $scope.projectDetails = response;
      console.log($scope.projectDetails);
    },
    function(response) {      
      console.log("Could not load projects!");
    }
  );

  $scope.addProject = function() {
    console.log("add new project");
    projectService.add($scope.token).save({name: $scope.projectName}).$promise.then(
      function(response) {
        console.log("Project added successfully!");
      },
      function(response) {
        console.log("Error: cannot  add new project");
      }
    )
  }

  $scope.addMethod = function(name) {
    projectService.addmethod($scope.token, name).save($scope.method).$promise.then(
      function(response) {
        console.log(response);
        console.log("Method successfully added!");
      },
      function(response) {
        console.log("Could not add method!");
      }
    )
  }

  $scope.editMethod = function(api, name) {
    projectService.editmethod($scope.token, name).update({url: api.url, method: api.method, requestBody: api.requestBody, responseBody: api.responseBody, optionalHeaders: api.optionalHeaders}).$promise.then(
      function(response) {
        console.log("Method successfully updated");
      },
      function(response) {
        console.log("Method could not be updated!");
      }
    )
  }

  

  $scope.addUser = function(name) {
    console.log($scope.user);
    projectService.adduser($scope.token, name).save($scope.user).$promise.then(
      function(response) {
        console.log("User added suceessfully");
      },
      function(response) {
        console.log("User could not be added.");
      }
    )
  }
})