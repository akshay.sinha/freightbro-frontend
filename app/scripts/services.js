'use strict';

var app = angular.module('myApp');

app.constant('baseURL', 'http://localhost:3000/lostman/')
.factory('registerService', function (baseURL, $resource) {
  return $resource(baseURL+'signup');
})
.factory('loginService', function (baseURL, $resource) {
  return $resource(baseURL+'login');
})
.factory('userService', function(baseURL, $resource) {
  return {
    users: function(token) {
      return $resource(baseURL+'users', null, {
        query: {
          method: 'GET',
          isArray: true,
          headers: { 'Authorization': 'Bearer ' + token}
        }
      }) 
    }
  }
})
.factory('projectService', function(baseURL, $resource) {
  return {
    projects: function(token) {
      return $resource(baseURL+'project', null, {
        query: {
          method: 'GET',
          isArray: true,
          headers: { 'Authorization': 'Bearer ' + token}
        }
      }) 
    },
    add: function(token) {
      return $resource(baseURL+'project', null, {
        save: {
          method: 'POST',
          headers: { 'Authorization': 'Bearer ' + token}
        }
      }) 
    },
    addmethod: function(token, name) {
      return $resource(baseURL+'project/'+name, null, {
        save: {
          method: 'POST',
          headers: { 'Authorization': 'Bearer ' + token}
        }
      }) 
    },
    editmethod: function(token, name) {
      return $resource(baseURL+'project/'+name, null, {
        update: {
          method: 'PUT',
          headers: { 'Authorization': 'Bearer ' + token}
        }
      })
    },
    adduser: function(token, name) {
      return $resource(baseURL+'adduser/'+name, null, {
        save: {
          method: 'POST',
          headers: { 'Authorization': 'Bearer ' + token}
        }
      })
    }
  }
})
